#!/bin/sh


set -e

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# nginx by default runs as a background application
# Docker recommends running the primary application in the foreground
# so that we get logs in the stdout, to the docker output
nginx -g 'daemon off;'
